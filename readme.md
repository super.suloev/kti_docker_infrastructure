# Login webapp for Docker containers (Flask WSGI server + nginx + PostgreSQL)

## Setup and installation

Create a "setup.sh" file in your home directory

setup.sh
```sh
#!/bin/bash

printf '%s %s\n' "Start deployment in" "$(date)"
printf '%s\n' "Cloning infrastructure files from repo"
git clone https://gitlab.com/super.suloev/kti_docker_infrastructure.git kti-infrastructure
cd kti-infrastructure 
printf '%s\n' "Cloning development files from repo"
git clone https://gitlab.com/super.suloev/kti_flask_auth_postgres.git flask_app
printf '%s %s\n' "Start building custom flask image in" "$(date)"
docker build -t "suloevsa/flask:1.0" .
printf '%s %s\n' "Start containers in" "$(date)"
docker compose up -d
printf '%s %s\n' "Infrastructure ready in" "$(date)"
```
